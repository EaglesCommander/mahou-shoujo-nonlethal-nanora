extends Node2D


var starlit = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if starlit:
		$AnimationPlayer.play("Starlit")
	else:
		$AnimationPlayer.play("Fire")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
