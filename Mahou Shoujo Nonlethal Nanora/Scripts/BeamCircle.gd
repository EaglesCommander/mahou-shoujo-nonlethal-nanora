extends Node2D

var laser_scene = load("res://Scenes/EnemyLaser2.tscn")
var bullet_scene = load("res://Scenes/EnemyBullet2.tscn")
var beam_scene = load("res://Scenes/BeamEnemy.tscn")
var explode = load("res://Scenes/Explode1.tscn")
var counter = 0
var number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if number == 0:
		$BeamTimer.set_wait_time(4)
		$BeamTimer.start()
	elif number == 1:
		$BeamTimer.set_wait_time(8)
		$BeamTimer.start()
	
	var ex = explode.instance()
	ex.scale.x = 0.5
	ex.scale.y = 0.5
	self.add_child(ex)

func _on_Timer_timeout():
	counter += 8
	
	if counter >= 360:
		counter = 0
	if number == 0:
		for n in range (10):
			var b = laser_scene.instance()
			b.target = deg2rad(36 * n + counter)
			b.position = self.global_position
			get_parent().add_child(b)
		
		$Laser.play()
	
	else:
		for n in range (10):
			var b = bullet_scene.instance()
			b.target = deg2rad(36 * n + counter)
			b.position = self.global_position
			get_parent().add_child(b)
		
		$Bullet.play()
		
func _on_BeamTimer_timeout():
	$Charging.play()
	$Beaming.start()
	var enemy = get_parent().get_node_or_null("Player")
	if enemy == null:
		enemy = get_parent().get_node_or_null("Screen")
	
	var b = beam_scene.instance()
	b.target = get_angle_to(enemy.global_position)
	b.position = self.global_position
	get_parent().add_child(b)
	
	$BeamTimer.set_wait_time(8)
	$BeamTimer.start()

func death():
	var ex = explode.instance()
	ex.scale.x = 0.5
	ex.scale.y = 0.5
	ex.position = self.global_position
	get_parent().add_child(ex)
	queue_free()

func _on_Beaming_timeout():
	$Beam.play()
