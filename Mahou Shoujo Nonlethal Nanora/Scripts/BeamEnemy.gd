extends Node2D


var starlit = false
var target = null

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rotation = target
	if starlit:
		$AnimationPlayer.play("Starlit")
	else:
		$AnimationPlayer.play("Fire")

func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
