extends Node2D


var target = null
var velocity = Vector2(0, 0)
var explode = load("res://Scenes/Explode1.tscn")

export (int) var speed = 400

# Called when the node enters the scene tree for the first time.
func _ready():
	target = get_angle_to(get_parent().get_node("Player").global_position)

func _process(delta):
	$Character/Center.rotate(deg2rad(10))
	if $Character/Center.rotation_degrees >= 360:
		$Character/Center.rotation_degrees = 0
		
	velocity.x = cos(target)
	velocity.y = sin(target)
	global_position += velocity * speed * delta
	
	if $Character/Center.rotation_degrees/180 < 1:
		$Character.flip_h = false
	else:
		$Character.flip_h = true

func _on_VisibilityNotifier2D_screen_exited():
	target = get_angle_to(get_parent().get_node("Player").global_position)
	
func death():
	var ex = explode.instance()
	ex.position = self.global_position
	get_parent().add_child(ex)
	queue_free()
