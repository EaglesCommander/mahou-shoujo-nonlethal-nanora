extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target = null
var velocity = Vector2(0, 0)

export (int) var speed = 400

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.x = cos(target)
	velocity.y = sin(target)
	global_position += velocity * speed * delta


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
