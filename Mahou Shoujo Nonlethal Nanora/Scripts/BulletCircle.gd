extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var bullet_scene = load("res://Scenes/EnemyLaser.tscn")
var explode = load("res://Scenes/Explode1.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(0.5)
	$Timer.start()
	var ex = explode.instance()
	ex.scale.x = 0.5
	ex.scale.y = 0.5
	self.add_child(ex)

func _on_Timer_timeout():
	var enemy = get_parent().get_node_or_null("Player")
	if enemy == null:
		enemy = get_parent().get_node_or_null("Screen")
	var b = bullet_scene.instance()
	b.position = self.global_position
	b.target = get_angle_to(enemy.global_position)
	
	b.speed = 800
	get_parent().add_child(b)
	$Laser.play()
