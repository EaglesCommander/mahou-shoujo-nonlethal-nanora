extends Node2D

var laser_scene = load("res://Scenes/EnemyLaser2.tscn")
var laser_scene2 = load("res://Scenes/EnemyLaser3.tscn")
var explode = load("res://Scenes/Explode1.tscn")
var counter = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Laser.play()
	var ex = explode.instance()
	ex.scale.x = 0.5
	ex.scale.y = 0.5
	self.add_child(ex)

func _on_Timer_timeout():
	counter += 8
	
	if counter >= 360:
		counter = 0
	
	for n in range (4):
		var b = laser_scene.instance()
		b.target = deg2rad(90 * n + counter)
		b.position = self.global_position
		get_parent().add_child(b)
		var b2 = laser_scene2.instance()
		b2.target = deg2rad((90 * n) + counter)
		b2.position = self.global_position
		get_parent().add_child(b2)
		
	$Laser.play()

func death():
	var ex = explode.instance()
	ex.scale.x = 0.5
	ex.scale.y = 0.5
	self.add_child(ex)
	queue_free()
