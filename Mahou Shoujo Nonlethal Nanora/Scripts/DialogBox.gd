# DialogBox.gd
extends RichTextLabel

# Variables
var dialog = [
	"Nanora: Ah, another day of gathering jewel seeds, I wonder just how many did Yuknow-kun lost...",
	"Nanora: This is no place for a normal third grader like me should hang out!",
	"Nanora: I should be out there searching for cute girls right now, aaaah, why did I agree to do this?",
	"Kate: ...",
	"Nanora: Huh, another girl, I see, this must be a dream.",
	"Kate: ...Fellow seeker of the lost logia, consider this as a warning, back down.",
	"Nanora: She talked! What a scary hallucination.",
	"Kate: I'm serious, back off before you get hurt.",
	"Nanora: Whoa, now that I look closer you're pretty cute.",
	"Kate: Ignored huh, well, don't forget that I warned you.",
	"Nanora: Wait, what's your name? I'm Nanora, Hikumura Nanora.",
	"Kate: ...Do you think I could just tell you like that?",
	"Nanora: Hey, what do you say if we get out of this forest and go somewhere nice, I know a good place.",
	"Kate: You really need to learn to listen.",
	"Nanora: Come on, let's go, it'll be my treat!",
	"Kate: No, I have no busine-",
	"Nanora: Playing hard to get huh, well no problem then....",
	"Kate: ....?",
	"Nanora: I'll just have to make you understand... by force!",
	"Kate: You....!",
	""]

var page = 0

# Functions
func _ready():
	set_process_input(true)
	set_bbcode(dialog[page])
	set_visible_characters(0)

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
