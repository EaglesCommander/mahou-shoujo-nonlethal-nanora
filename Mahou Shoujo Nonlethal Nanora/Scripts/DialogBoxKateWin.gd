# DialogBox.gd
extends RichTextLabel

# Variables
var dialog = [
	"Nanora: Ah...I may have overdone it...",
	"Nanora: Man, now she's nowhere to be seen.",
	"Nanora: Idiot me, remember, girls love seeing my Starlit Breaker.",
	"Nanora: But it only appears when both me and my target are at critical conditions anyway...",
	"Nanora: Oh well, no use dawdling on the past, time to get back to work.",
	"Nanora: ...",
	"Nanora: That girl was really cute, though.",
	"Nanora: I wish I could turn back time and re-do that battle...",
	"Tips: Use Starlit Breaker to make your enemies friendly!",
	""]

var page = 0

# Functions
func _ready():
	set_process_input(true)
	set_bbcode(dialog[page])
	set_visible_characters(0)

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
