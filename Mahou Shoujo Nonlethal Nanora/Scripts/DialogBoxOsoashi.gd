# DialogBox.gd
extends RichTextLabel

# Variables
var dialog = [
	"Nanora: Phew, Bitta-chan sure is strong, but I sure made her understand!",
	"Nanora: I would've loved to add her to my colle- err... contact list...?",
	"Nanora: Yes! Contact list! But she's a bit too... angsty... for my taste",
	"Osoashi: ...",
	"Nanora: Huh? Osoashi-chan? What are you doi-",
	"Book: Hostile detected.",
	"Nanora: Whoa, Osoashi-chan, the book just talked you know!?",
	"Osoashi: ...",
	"Nanora: Wait, I guess a normal person couldn't fly either, ahaha.",
	"Osoashi: ...",
	"Nanora: Come on, that was totally a good joke!",
	"Book: Proceeding to eliminate hostile enemy.",
	"Nanora: Hold on there, I can't let you beat me up pal.",
	"Osoashi: ...",
	"Nanora: Cold. But I guess I don't mind Kuudere type girls.",
	"Book: Activate.",
	"Nanora: I just have to show you how warm I can get...",
	"Book: Registration complete, welcome, master Osoashi",
	"Nanora: ...with my BREAKER",
	"Osoashi: ...!",
	""]

var page = 0

# Functions
func _ready():
	set_process_input(true)
	set_bbcode(dialog[page])
	set_visible_characters(0)

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
