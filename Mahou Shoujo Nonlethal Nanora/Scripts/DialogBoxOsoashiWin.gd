extends RichTextLabel

# Variables
var dialog = [
	"Nanora: Ah, crap, I didn't mean to do that.",
	"Nanora: Man, I have to keep my power in check sometimes.",
	"Nanora: A disintigrated girl just as good as a guy.",
	"Nanora: Or as bad...?",
	"Nanora: ...",
	"Nanora: I have a feeling that there's nothing I could do anyway.",
	"Nanora: Though maybe, someday in the future, we will meet again",
	"Nanora: Maybe...",
	"Nanora: ...",
	"Nanora: I'm hungry...",
	"System: This is the current end of the game",
	"System: As of right now, there is no way to unlock Osoashi",
	"System: You will be returned to the main menu.",
	"System: Thank you for playing!",
	""]

var page = 0

# Functions
func _ready():
	set_process_input(true)
	set_bbcode(dialog[page])
	set_visible_characters(0)

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
