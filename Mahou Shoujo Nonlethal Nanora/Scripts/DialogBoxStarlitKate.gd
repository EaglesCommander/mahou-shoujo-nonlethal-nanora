# DialogBox.gd
extends RichTextLabel

# Variables
var dialog = [
	"Nanora: Phew, I'm beat.",
	"Kate: .......",
	"Nanora: How was that? Pretty cool right?",
	"Kate: .......",
	"Nanora: Ah, was it too much stimulation?",
	"Kate: ...Who... are you?",
	"Nanora: Just your ordinary third grader, Hikumura Nanora.",
	"Nonora: Hey, you haven't told me yours now.",
	"Kate: Why would I...",
	"Nanora: Oh, it seems that my point hasn't reached you yet then.",
	"Kate: ...Kate.",
	"Kate: Kate Maranello",
	"Nanora: Nice to meet you Kate-chan! So about that place I mentioned...",
	"Kate: I don't-",
	"Nanora: Hmm?",
	"Kate: I... would be glad to...",
	"Nanora: Ahahaha, let's go then. Ah, you can call me darling, okay?",
	"Nanora: Or by my name, I'd be happier with the former though :)",
	"Kate: ...",
	"Nanora: I'm waiting",
	"Kate: Na... Nanora",
	"Nanora: Aha, let's go, Kate-chan",
	"Kate: ...",
	"Kate: ...She's really strong",
	"You have unlocked Kate! To play a level with her, choose her with on Select Level!",
	""]

var page = 0

# Functions
func _ready():
	set_process_input(true)
	set_bbcode(dialog[page])
	set_visible_characters(0)

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
