extends Node2D

var target = null
var velocity = Vector2(0, 0)
var original = null
var bullet = load("res://Scenes/EnemyBullet2.tscn")
var explode = true

export (int) var speed = 50

# Called when the node enters the scene tree for the first time.
func _ready():
	original = self.global_position

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.x = cos(target)
	velocity.y = sin(target)
	global_position += velocity * speed * delta
	
	if self.global_position.distance_to(original) >= 500 and explode:
		for n in range(10):
			var b = bullet.instance()
			b.target = (36 * n)
			b.position = self.global_position
			get_parent().add_child(b)
		
		queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
