extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target = null
var velocity = Vector2(0, 0)

export (int) var speed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rotation = target + deg2rad(90)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	speed += 2
	velocity.x = cos(target)
	velocity.y = sin(target)
	global_position += velocity * speed * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
