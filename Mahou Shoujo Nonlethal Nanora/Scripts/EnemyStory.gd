extends KinematicBody2D

export (int) var speed = 200
export (int) var jump_speed = -400
const UP = Vector2(0,-1)
var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var weapon = self.get_node("Character/Weapon")
onready var enemy = get_parent().get_node("Player")

func get_input(delta):
	velocity.x = 0
	velocity.y = 0
	
	self.position.x = enemy.position.x * -1
	self.position.y = enemy.position.y * -1

func _physics_process(delta):
	get_input(delta)
	velocity = move_and_slide(velocity, UP)

func _process(_delta):
	enemy = get_parent().get_node_or_null("Player")
	follow_mouse()
	if velocity.x != 0:
		if velocity.x > 0:
			if self.get_node("Character").flip_h:
				animator.play("Backward")
			else:
				animator.play("Forward")
		else:
			if self.get_node("Character").flip_h:
				animator.play("Forward")
			else:
				animator.play("Backward")
	else:
		animator.play("Idle")

func follow_mouse():
	if enemy.global_position.x > self.global_position.x:
		$Character.flip_h = false
		weapon.flip(false)
		weapon.position.x = 7
		
	else:
		$Character.flip_h = true
		weapon.flip(true)
		weapon.position.x = -7
	
	weapon.rotation_degrees = rad2deg(self.global_position.angle_to_point(enemy.global_position)) + 180
