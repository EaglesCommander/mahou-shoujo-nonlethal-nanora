extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target = null
var initial

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rotation_degrees = rad2deg(self.global_position.angle_to_point(target)) + 180
	initial = self.rotation_degrees

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.rotation_degrees < initial + 180:
		self.rotation_degrees += 10
	else:
		get_parent().remove_child(self)
