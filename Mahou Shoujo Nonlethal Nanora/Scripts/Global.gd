extends Node2D


var kate_unlocked = false
var osoashi_unlocked = false
var god_mode = false


# Called when the node enters the scene tree for the first time.
func _ready():
	load_game()

func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var data = {
		"kate" : kate_unlocked,	
		"osoashi" : osoashi_unlocked,
		"godmode" : god_mode
	}
	save_game.store_line(to_json(data))
	save_game.close()

func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return
		
	save_game.open("user://savegame.save", File.READ)
	
	var data = parse_json(save_game.get_line())
	
	kate_unlocked = data["kate"]
	osoashi_unlocked = data["osoashi"]
	god_mode = data["godmode"]
