extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -400
export (int) var lives = 5

const UP = Vector2(0,-1)

var velocity = Vector2()
var counter = 2
var dash = 0
var charName = "Kate"
var charging = false
var invisible = false
var isDead = false
var bullet_scene = load("res://Scenes/LaserKate.tscn")
var explosion = load("res://Scenes/Explode1.tscn")
var sword_scene = load("res://Scenes/SwordKate.tscn")
var focus = false

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Character")
onready var weapon = self.get_node("Character/Weapon")
onready var enemy = get_parent().get_node("Enemy")
onready var globvar = get_node("/root/Global")

func get_input(delta):
	velocity.x = 0
	velocity.y = 0
	focus = false
	
	if charging == true:
		return
	
	if !$Swing.playing:
		weapon.visible = true
	
	if Input.is_action_just_pressed('special') and !$Swing.playing:
		if get_parent().alternate:
			weapon.starlit = true
		weapon.visible = false
		charging = true
		$Dash.play()
		$ChargeTimer.start()
		return
	
	if Input.is_action_just_released('fire'):
		weapon.stop()
		
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_pressed('up'):
		velocity.y -= speed
	if Input.is_action_pressed('down'):
		velocity.y += speed
	
	if Input.is_action_just_pressed('shift') and (velocity.x != 0 or velocity.y != 0) and dash == 0:
		dash = 1
		invisible = true
		$Dash.play()
		$DashTimer.set_wait_time(0.1)
		$DashTimer.start()
	
	if dash == 1:
		velocity.x *= 7
		velocity.y *= 7
	
	if Input.is_action_pressed("focus"):
		focus = true
		velocity.x /= 2
		velocity.y /= 2
	
	if Input.is_action_pressed('fire') and $BulletTimer.is_stopped() and !$Swing.playing:
		$BulletTimer.start()
		weapon.fire()
		var b = bullet_scene.instance()
		b.position = weapon.get_node("Point").global_position
		b.target = get_angle_to(get_global_mouse_position())
		if focus:
			b.target = get_angle_to(enemy.global_position)
		b.speed = 1000
		get_parent().add_child(b)
		$Shoot.play()

func _physics_process(delta):
	get_input(delta)
	velocity = move_and_slide(velocity, UP)

func _process(_delta):
	enemy = get_parent().get_node_or_null("Enemy")
	if enemy == null:
		enemy = get_parent().get_node_or_null("Screen")
	follow_mouse()
	
	if charging or $Swing.playing:
		if enemy.position.x > self.position.x:
			$Character.flip_h = false
			weapon.flip(false)
			weapon.position.x = 7
			
		else:
			$Character.flip_h = true
			weapon.flip(true)
			weapon.position.x = -7
		return
	
	if velocity.x != 0:
		if velocity.x > 0:
			if self.get_node("Character").flip_h:
				animator.play("Backward")
			else:
				animator.play("Forward")
		else:
			if self.get_node("Character").flip_h:
				animator.play("Forward")
			else:
				animator.play("Backward")
	else:
		animator.play("Idle")

func follow_mouse():
	if focus:
		if enemy.global_position.x > self.position.x:
			$Character.flip_h = false
			weapon.flip(false)
			weapon.position.x = 7
			
		else:
			$Character.flip_h = true
			weapon.flip(true)
			weapon.position.x = -7
	else:
		if get_global_mouse_position().x > self.position.x:
			$Character.flip_h = false
			weapon.flip(false)
			weapon.position.x = 7
			
		else:
			$Character.flip_h = true
			weapon.flip(true)
			weapon.position.x = -7
	
	weapon.rotation_degrees = rad2deg(self.position.angle_to_point(get_global_mouse_position())) + 180
	if weapon.starlit or focus:
		weapon.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position)) + 180

func _on_Core_area_entered(area):
	if invisible || charging:
		return
		
	if "Bitta" in area.get_name() && !charging:
		lives -= 1
		invisible = true
	if "EnemyBullet" in area.get_parent().get_name() && !charging:
		lives -= 1
		invisible = true
	if "EnemySword" in area.get_parent().get_name() && !charging:
		lives -= 1
		invisible = true
	if "EnemyLaser" in area.get_parent().get_name() && !charging:
		lives -= 1
		invisible = true
		
	if ("Bullet" in area.get_parent().get_name() || "Enemy" in area.get_parent().get_name()) && !invisible:
		return
	
	death()
	
	if lives <= 0 && !globvar.god_mode:
		isDead = true
	
func death():
	$Death.play()
	var ex = explosion.instance()
	ex.position = self.global_position
	get_parent().add_child(ex)
	
		
	for x in get_parent().get_children():
		if "EnemyBullet" in x.get_name():
			self.get_parent().remove_child(x)
		if "EnemyLaser" in x.get_name():
			self.get_parent().remove_child(x)
	
	$Character.modulate.a = 0.7
	$ReviveTimer.set_wait_time(2)
	$ReviveTimer.start()

func _on_ReviveTimer_timeout():
	$Character.modulate.a = 1
	invisible = false
	$ReviveTimer.stop()

func _on_DashTimer_timeout():
	if dash == 2:
		$DashTimer.stop()
		dash = 0
		return
	
	invisible = false
	dash = 2
	$DashTimer.set_wait_time(1)
	$DashTimer.start()

func _on_ChargeTimer_timeout():
	var target = Vector2(0, 0)

	if abs(self.global_position.x - enemy.global_position.x) <= 50 && abs(self.global_position.y - enemy.global_position.y) <= 50:
		$ChargeTimer.stop()
		$Character.modulate.a = 0.7
		$ReviveTimer.set_wait_time(2)
		$ReviveTimer.start()
		invisible = true
		charging = false
		slash()
		if get_parent().alternate and weapon.starlit:
			enemy.death()
			return
		return
		
	target.x = cos(get_angle_to(enemy.global_position))
	target.y = sin(get_angle_to(enemy.global_position))
	velocity = target * 40
	move_and_collide(velocity)
	
func slash():
	var s = sword_scene.instance()
	s.target = enemy.position
	self.add_child(s)
	
	$Swing.play()

func _on_Swing_finished():
	lives -= 1
	if lives > 0 or globvar.god_mode:
		invisible = true
	else:
		isDead = true
