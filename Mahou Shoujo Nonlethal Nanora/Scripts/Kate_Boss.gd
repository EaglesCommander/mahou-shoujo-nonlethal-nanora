extends KinematicBody2D

var health = 100
var pattern = 0
var pattern2 = 0
var pattern22 = 3
var pattern4 = 0
var dashsound = 0
var unlock_flag = false
var invisible = false
var intermission = false
var bullet_scene = load("res://Scenes/EnemyBullet.tscn")
var sword_scene = load("res://Scenes/EnemySword.tscn")
var circle_scene = load("res://Scenes/BulletCircle.tscn")
var circle2 = load("res://Scenes/BulletCircle2.tscn").instance()
var laser_scene = load("res://Scenes/EnemyLaser.tscn")
var explosion = load("res://Scenes/Explode1.tscn")
var explosion2 = load("res://Scenes/Explode2.tscn")
var velocity = Vector2()
var isDead = false
const UP = Vector2(0,-1)

onready var animator = self.get_node("Animator")
onready var weapon = self.get_node("Character/Weapon")
onready var enemy = get_parent().get_node("Player")


func _ready():
	$Timer.set_wait_time(0.01)
	$Timer.start()

func true_ready():
	$BulletTimer.set_wait_time(0.1)
	$BulletTimer.start()
	$Timer.set_wait_time(0.001)
	$Timer.start()
	$Character/Weapon.visible = false
	self.add_child(circle2)
	invisible = false

func _on_BulletTimer_timeout():
	if pattern == 1:
		pattern1()
	elif pattern == 2:
		pattern2()
	elif pattern == 3:
		pattern3()
	elif pattern == 4:
		pattern4()
		
func _on_Timer_timeout():
	if pattern == 0:
		scene_enter()
	if pattern == 1:
		pattern1mov()
	if pattern == 2:
		pattern2mov()
	if pattern == 3:
		pattern3mov()
	if pattern == 4:
		pattern4mov()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	enemy = get_parent().get_node_or_null("Player")
	if enemy == null:
		enemy = get_parent().get_node_or_null("Screen")
	follow_mouse()
	if velocity.x <= 0.0001 and velocity.x >= -0.0001:
		animator.play("Idle")
	elif abs(velocity.x) != 0:
		if velocity.x > 0:
			if self.get_node("Character").flip_h:
				animator.play("Backward")
			else:
				animator.play("Forward")
		else:
			if self.get_node("Character").flip_h:
				animator.play("Forward")
			else:
				animator.play("Backward")
func follow_mouse():
	if enemy.position.x > self.position.x:
		$Character.flip_h = false
		weapon.flip(false)
		weapon.position.x = 7
		
	else:
		$Character.flip_h = true
		weapon.flip(true)
		weapon.position.x = -7

	weapon.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position)) + 180

func pattern1():
	$Bullet.play()
		
	for n in range(5):
		var b = bullet_scene.instance()
		b.position = self.global_position
		b.target = get_angle_to($Fire/Target.global_position)
		get_parent().add_child(b)
		$Fire.rotate(deg2rad(72))
	
func pattern1mov():
	$Fire.rotate(0.9)

func pattern2():
	$Fire.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position))
	$Fire.rotate(deg2rad(-70))
	pattern22 -= 1
	
	if pattern22 <= 0:
		for n in range(7):
			var b = bullet_scene.instance()
			b.position = self.global_position
			b.target = get_angle_to($Fire/Target.global_position)
			b.speed = 200
			get_parent().add_child(b)
			$Fire.rotate(deg2rad(-10))
		pattern22 = 3
		
	var b = laser_scene.instance()
	b.position = $Character/Weapon/Point.global_position
	b.target = get_angle_to(enemy.global_position)
	b.speed = 800
	get_parent().add_child(b)
	
	$Laser.play()

func pattern2mov():
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	if pattern2 == 4:
		pattern2 = 0
	
	if pattern2 == 0:
		node = node.get_node("TopRight")
	elif pattern2 == 1:
		node = node.get_node("TopLeft")
	elif pattern2 == 2:
		node = node.get_node("BottomLeft")
	elif pattern2 == 3:
		node = node.get_node("BottomRight")
	
	if abs(self.global_position.x - node.global_position.x) <= 5 && abs(self.global_position.y - node.global_position.y) <= 5:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		pattern2 += 1
		return
		
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 4
	move_and_collide(velocity)
	
func pattern3():
	var s = sword_scene.instance()
	s.target = enemy.position
	self.add_child(s)
	$Fire.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position))
		
	for n in range(10):
		var b = bullet_scene.instance()
		b.position = self.global_position
		b.target = get_angle_to($Fire/Target.global_position)
		b.speed = 100
		get_parent().add_child(b)
		$Fire.rotate(deg2rad(-18))
	$BulletTimer.stop()
	
	$Swing.play()
	
func pattern3mov():
	var target = Vector2(0, 0)

	if abs(self.global_position.x - enemy.global_position.x) <= 50 && abs(self.global_position.y - enemy.global_position.y) <= 50:
		$Timer.stop()
		$Timer.set_wait_time(1)
		$Timer.start()
		$BulletTimer.start()
		dashsound = 0
	elif dashsound == 0:
		$Dash.play()
		dashsound = 1
	
	$Timer.set_wait_time(0.001)
		
	target.x = cos(get_angle_to(enemy.global_position))
	target.y = sin(get_angle_to(enemy.global_position))
	velocity = target * 40
	move_and_collide(velocity)
	
func pattern4():
	$Fire.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position))
	
	for n in range(10):
		var b = bullet_scene.instance()
		b.position = $Character/Weapon/Point.global_position
		b.target = get_angle_to($Fire/Target.global_position)
		b.speed = 200
		get_parent().add_child(b)
		$Fire.rotate(deg2rad(-18))
		
	$Bullet.play()
	
func pattern4mov():
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	
	if pattern4 == 3:
		pattern4 = 1
		
	if health <= 30 and pattern4 != 0:
		unlock_flag = true

	if pattern4 == 0:
		node = node.get_node("MiddleRight")
	elif pattern4 == 1:
		node = node.get_node("TopRight")
	elif pattern4 == 2:
		node = node.get_node("BottomRight")
	
	if abs(self.global_position.x - node.global_position.x) <= 5 && abs(self.global_position.y - node.global_position.y) <= 5 && pattern4 == 0:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		pattern4 += 1
		$BulletTimer.set_wait_time(0.33)
		$BulletTimer.start()
		var circle1 = circle_scene.instance()
		circle1.global_position = get_parent().get_node("Screen").get_node("TopRight").global_position
		get_parent().add_child(circle1)
		var circle2 = circle_scene.instance()
		circle2.global_position = get_parent().get_node("Screen").get_node("BottomRight").global_position
		get_parent().add_child(circle2)
		invisible = false
		return
	elif abs(self.global_position.x - node.global_position.x) <= 50 && abs(self.global_position.y - node.global_position.y) <= 50 && pattern4 != 0:
		pattern4 += 1
		return
		
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 4
	move_and_collide(velocity)

func _on_Intermission_timeout():
	$Intermission.stop()
	$Character.modulate.a = 1
	
	if intermission:
		$Intermission.set_wait_time(2)
		$Intermission.start()
		intermission = false
		pattern += 1
		self.add_child(explosion2.instance())
		$Recover.play()
		return
	if pattern == 2:
		health = 100
		self.remove_child(circle2)
		$Timer.set_wait_time(0.01)
		$Timer.start()
		$BulletTimer.set_wait_time(0.2)
		$BulletTimer.start()
		$Character/Weapon.visible = true
		$Character/Weapon/Sprite/Circle.visible = true
	elif pattern == 3:
		health = 100
		$Timer.stop()
		$Timer.set_wait_time(0.002)
		$Timer.start()
		$BulletTimer.set_wait_time(0.2)
		$BulletTimer.stop()
		$Character/Weapon.visible = false
	invisible = false
	
	if pattern == 4:
		health = 100
		$Timer.set_wait_time(0.01)
		$Timer.start()
		$BulletTimer.stop()
		$Character/Weapon.visible = true
		$Character/Weapon/Sprite/Circle.visible = true
		invisible = true
		
func death():
	var ex = explosion.instance()
	ex.position = self.global_position
	get_parent().add_child(ex)
	invisible = true
	intermission = true
	
	$Character.modulate.a = 0.7
	$Intermission.set_wait_time(2)
	$Intermission.start()
	$BulletTimer.stop()
	$Timer.stop()
	
	$Death.play()
	
	for x in get_parent().get_children():
		if "EnemyBullet" in x.get_name():
			self.get_parent().remove_child(x)
		if "EnemyLaser" in x.get_name():
			self.get_parent().remove_child(x)
		if "Circle" in x.get_name():
			self.get_parent().remove_child(x)
	
	if pattern == 4:
		isDead = true
		$Intermission.stop()
		return

func _on_Core_Fate_area_entered(area):
	if invisible:
		return

	if "EnemyBullet" in area.get_parent().get_name() or "EnemyLaser" in area.get_parent().get_name() or "EnemySword" in area.get_parent().get_name():
		return
	if "Bullet" in area.get_parent().get_name():
		self.get_parent().remove_child(area.get_parent())
		if pattern == 1:
			health -= 0.5
		elif pattern == 4:
			health -= 2
		else:
			health -= 2
	if "Beam" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		health -= 50
		$BeamTimer.start()
	if "Sword" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		health -= 70
		$BeamTimer.start()
	if "Laser" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		self.get_parent().remove_child(area.get_parent())
		health -= 5
	if health <= 0:
		death()

func scene_enter():
	invisible = true
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	
	if abs(self.global_position.x - node.global_position.x) <= 2 && abs(self.global_position.y - node.global_position.y) <= 2:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		true_ready()
		pattern += 1
		return
	
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 2
	move_and_collide(velocity)
