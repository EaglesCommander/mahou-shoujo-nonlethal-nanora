extends Node2D

var win_scene = load("res://Scenes/WinMenuOsoashi.tscn")
var lose_scene = load("res://Scenes/LoseMenuOsoashi.tscn")
var pause_scene = load("res://Scenes/Pause.tscn")
var explosion = load("res://Scenes/Explode1.tscn")
var end = false
var alternate = false
var charName = "Null"
var prev_pattern = 0

onready var globvar = get_node("/root/Global")

func _ready():
	$Timer.set_wait_time(120)

func _process(delta):
	if end:
		return
		
	if Input.is_action_just_pressed('ui_cancel'):
		var pause = pause_scene.instance()
		pause.position = $Screen.position
		pause.z_index = 1
		self.add_child(pause)
		
	if $Background2.position.x <= -291:
		$Background2.position.x = $StartPoint.position.x
	if $Background1.position.x <= -291:
		$Background1.position.x = $StartPoint.position.x
	$Background2.position.x -= 1
	$Background1.position.x -= 1
	
	if $Enemy.isDead:
		deadprep()
		end = true
		remove_child($Enemy)
		$Animwait.start()
		return
	elif $Player.isDead:
		deadprep()
		_on_Timer_timeout()
		return
	
	if $Caution.position.y <= -419:
		$Caution.position.y = $StartPoint2.position.y
	if $Caution2.position.y <= -419:
		$Caution2.position.y = $StartPoint2.position.y
	if $Caution3.position.y <= -419:
		$Caution3.position.y = $StartPoint2.position.y
		
	if $Caution4.position.y >= 419:
		$Caution4.position.y = $StartPoint3.position.y
	if $Caution5.position.y >= 419:
		$Caution5.position.y = $StartPoint3.position.y
	if $Caution6.position.y >= 419:
		$Caution6.position.y = $StartPoint3.position.y
	
	$Caution.position.y -= 1
	$Caution2.position.y -= 1
	$Caution3.position.y -= 1
	$Caution4.position.y += 1
	$Caution5.position.y += 1
	$Caution6.position.y += 1
	
	$Layer/GUI/Top/Left/Life/Count.text = str($Player.lives)
	if globvar.god_mode:
		$Layer/GUI/Top/Left/Life/Count.text = "∞"
	$Layer/GUI/Top/Left/Lifebar.value = $Enemy.health
	$Layer/GUI/Bottom/HBoxContainer/TextureRect2/Time.text = str(int($Timer.time_left))
	if $Layer/GUI/Bottom/HBoxContainer/TextureRect2/Time.text == "0":
		$Layer/GUI/Bottom/HBoxContainer/TextureRect2/Time.text = "-"
	$Player.pause_mode = false
	
	if $Enemy.intermission:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Osoashi is Recovering"
	elif $Enemy.pattern == 0:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Osoashi - Devilish Angel"
	elif $Enemy.pattern == 1:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Nibelungenlied - Balmung"
	elif $Enemy.pattern == 2:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Wolkenritter - Bitta"
	elif $Enemy.pattern == 3:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Unison - Diabolic Emission"
	elif $Enemy.pattern == 4:
		$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "Gjallarhorn - Ragnarok"
	
	if $Player.charName == "Nanora":
		if $Player.charging:
			$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "DIVINITY BUSTER"
		charName = "Nanora"
		if ($Player.lives == 1 or globvar.god_mode) and $Enemy.unlock_flag and !$Screen/SB/Clink.playing and !$Player/Character/Weapon.starlit:
			$Screen/SB.visible = true
			$Screen/SB/Clink.play()
			alternate = true

		if alternate and $Player.charging and !$Screen/SB/Clink.playing and !$Player/Character/Weapon.starlit:
			$Screen/SB/Clink.play()

		if $Player.charging and $Player/Character/Weapon.starlit:
			$"Screen/SB/SB-Text".text = "STARLIT BREAKER"
			deadprep()
			
	elif $Player.charName == "Kate":
		if  $Player.charging:
			$Layer/GUI/Bottom/HBoxContainer/TextureRect/Phase.text = "SONIC MOVE"
		charName = "Kate"
		if ($Player.lives == 1 or globvar.god_mode) and $Enemy.unlock_flag and !$Screen/SB/Clink.playing and !$Player/Character/Weapon.starlit:
			$Screen/SB.visible = true
			$Screen/SB/Clink.play()
			alternate = true

		if alternate and $Player.charging and !$Screen/SB/Clink.playing and !$Player/Character/Weapon.starlit:
			$Screen/SB/Clink.play()

		if $Player.charging and $Player/Character/Weapon.starlit:
			$"Screen/SB/SB-Text".text = "JET ZANBER"
			deadprep()
	
	if $Enemy.pattern != prev_pattern && !$Enemy.invisible:
		$Timer.start()
		prev_pattern += 1
	
	if $Enemy.intermission:
		$Timer.stop()

func _on_BGM_finished():
	$BGM.stream = load("res://Assets/Music/Battle-Unison_loop.ogg")
	$BGM.play()

func _on_Timer_timeout():
	if $Timer.wait_time > 4:
		$Timer.set_wait_time(3)
		$Timer.start()
		var ex = explosion.instance()
		ex.position = $Player.global_position
		self.add_child(ex)
		remove_child($Player)
		end = true
		return
		
	var lose = lose_scene.instance()
	lose.position = $Screen.position
	lose.z_index = 1
	self.add_child(lose)
	
func _on_Animwait_timeout():
	var win = win_scene.instance()
	win.position = $Screen.position
	if alternate:
		win.alternate = true
	win.z_index = 1
	self.add_child(win)

func deadprep():
	$Layer.visible = false
	for x in self.get_children():
		if "Caution" in x.get_name():
			x.visible = false
