extends MarginContainer


var next_scene = load("res://Scenes/WinOsoashi.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(0.005)
	$Timer.start()

func _on_Timer_timeout():
	$Menu/Logo/LogoContainer/TextureProgress.value += 2
	
	if $Menu/Logo/LogoContainer/TextureProgress.value == 100:
		$Timer.stop()
		get_tree().change_scene_to(next_scene)
