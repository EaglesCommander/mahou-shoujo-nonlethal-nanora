extends Node2D

var menu = load("res://Scenes/MainMenu.tscn")
var level2 = load("res://Scenes/LevelOsoashi.tscn")
var level2alt = load("res://Scenes/LevelOsoashi2.tscn")
var winstory = load("res://Scenes/LoadingWinOsoashi.tscn")
var winstorysb = load("res://Scenes/LoadingWinOsoashi.tscn")
var alternate = false
onready var globvar = get_node("/root/Global")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().get_tree().paused = true

func _on_TextureButton_button_down():
	pass # Replace with function body.

func _on_Resume_button_down():
	get_parent().get_tree().paused = false
	get_parent().remove_child(self)

func _on_Quit_button_down():
	get_parent().get_tree().quit()

func _on_MainMenu_button_down():
	get_parent().get_tree().paused = false
	get_parent().get_tree().change_scene_to(menu)

func _on_Retry_button_down():
	get_parent().get_tree().paused = false
	if get_parent().charName == "Nanora":
		get_parent().get_tree().change_scene_to(level2)
	else:
		get_parent().get_tree().change_scene_to(level2alt)

func _on_Continue_button_down():
	get_parent().get_tree().paused = false
	if get_parent().charName == "Nanora":
		if alternate:
			get_parent().get_tree().change_scene_to(winstorysb)
		else:
			get_parent().get_tree().change_scene_to(winstory)
	else:
		get_parent().get_tree().change_scene_to(menu)
