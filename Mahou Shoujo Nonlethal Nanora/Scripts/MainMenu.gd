extends MarginContainer

var levelKate2 = load("res://Scenes/LoadingLevelKate2.tscn")
var levelOsoashi2 = load("res://Scenes/LoadingLevelOsoashi2.tscn")
var storyKate = load("res://Scenes/LoadingStoryKate.tscn")
var storyOsoashi = load("res://Scenes/LoadingStoryOsoashi.tscn")
var condition = 0
var title = 0
onready var globvar = get_node("/root/Global")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Menu/Settings/HBoxContainer/VBoxContainer/CheckBox.pressed = globvar.god_mode
	
func _input(event):
	if (event is InputEventKey || event is InputEventMouseButton) && condition == 0:
		if event.pressed: 
			$Menu/Logo/LogoContainer/Label.visible = false
			$Menu/MainOptions.visible = true
			title = 1
			condition = 1
	
	if event.as_text() == "Escape" && condition == 2:
		if event.pressed:
			$Menu/MainOptions.visible = true
			$Menu/MainOptions/Settings.disabled = false
			$Menu/MainOptions/NewGame.disabled = false
			$Menu/MainOptions/SelectLevel.disabled = false
			$Menu/MainOptions/Quit.disabled = false
			
			$Menu/SelectLevel.visible = false
			$"Menu/SelectLevel/Level 1".disabled = true
			$"Menu/SelectLevel/Level 2".disabled = true
			$Menu/SelectLevel/BackMain.disabled = true
			condition = 1
	
	if event.as_text() == "Escape" && condition == 3:
		if event.pressed:
			$"Menu/SelectLevel/Level 1".disabled = false
			$"Menu/SelectLevel/Level 2".disabled = false
			$Menu/SelectLevel/BackMain.disabled = false
			$Menu/SelectLevel.visible = true
			
			$Menu/Level2.visible = false
			$Menu/Level2/Nanora2.disabled = true
			$Menu/Level2/Kate2.disabled = true
			$Menu/Level2/BackLevel.disabled = true
			condition = 2
			
	if event.as_text() == "Escape" && condition == 4:
		if event.pressed:
			$Menu/MainOptions.visible = true
			$Menu/MainOptions/NewGame.disabled = false
			$Menu/MainOptions/SelectLevel.disabled = false
			$Menu/MainOptions/Quit.disabled = false
			$Menu/MainOptions/Settings.disabled = false

			$Menu/Settings/HBoxContainer/VBoxContainer/CheckBox.disabled = true
			$Menu/Settings/CenterContainer/BackMain2.disabled = true
			$Menu/Settings.visible = false
			condition = 1
			globvar.save_game()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if globvar.kate_unlocked == false:
		$Menu/Level2/Kate2.disabled = true
		$Menu/Level2/Kate2.visible = false
		$Menu/Level1/Kate.disabled = true
		$Menu/Level1/Kate.visible = false
	if globvar.osoashi_unlocked == false:
		$"Menu/SelectLevel/Level 2".disabled = true
		$"Menu/SelectLevel/Level 2".visible = false
	
	if Input.is_action_just_pressed("fire") or Input.is_action_just_pressed("ui_cancel"):
		$Sound.stream = load("res://Assets/Menu/click1.ogg")
		$Sound.play()
	
	if title == 1:
		$Menu/MainOptions/NewGame.disabled = false
		$Menu/MainOptions/SelectLevel.disabled = false
		$Menu/MainOptions/Quit.disabled = false
		$Menu/MainOptions/Settings.disabled = false
		title = 2

func _on_NewGame_button_down():
	get_tree().change_scene_to(storyKate)

func _on_BGM_finished():
	$BGM.stream = load("res://Assets/Menu/MainMenuLoop.wav")
	$BGM.play()

func _on_SelectLevel_button_down():
	$Menu/MainOptions.visible = false
	$Menu/MainOptions/NewGame.disabled = true
	$Menu/MainOptions/SelectLevel.disabled = true
	$Menu/MainOptions/Quit.disabled = true
	$Menu/MainOptions/Settings.disabled = true
	
	$"Menu/SelectLevel/Level 1".disabled = false
	$"Menu/SelectLevel/Level 2".disabled = false
	$Menu/SelectLevel/BackMain.disabled = false
	$Menu/SelectLevel.visible = true
	condition = 2

func _on_BackMain_button_down():
	$Menu/MainOptions.visible = true
	$Menu/MainOptions/NewGame.disabled = false
	$Menu/MainOptions/SelectLevel.disabled = false
	$Menu/MainOptions/Quit.disabled = false
	$Menu/MainOptions/Settings.disabled = false
	
	$Menu/SelectLevel.visible = false
	$"Menu/SelectLevel/Level 1".disabled = true
	$"Menu/SelectLevel/Level 2".disabled = true
	$Menu/SelectLevel/BackMain.disabled = true
	condition = 1

func _on_BackLevel_button_down():
	$"Menu/SelectLevel/Level 1".disabled = false
	$"Menu/SelectLevel/Level 2".disabled = false
	$Menu/SelectLevel/BackMain.disabled = false
	$Menu/SelectLevel.visible = true
	
	$Menu/Level2.visible = false
	$Menu/Level2/Nanora2.disabled = true
	$Menu/Level2/Kate2.disabled = true
	$Menu/Level2/BackLevel.disabled = true
	
	$Menu/Level1.visible = false
	$Menu/Level1/Nanora.disabled = true
	$Menu/Level1/Kate.disabled = true
	$Menu/Level1/BackLevel.disabled = true
	condition = 1

func _on_Level_1_button_down():
	$Menu/SelectLevel.visible = false
	$"Menu/SelectLevel/Level 1".disabled = true
	$"Menu/SelectLevel/Level 2".disabled = true
	$Menu/SelectLevel/BackMain.disabled = true
	
	$Menu/Level1.visible = true
	$Menu/Level1/Nanora.disabled = false
	$Menu/Level1/Kate.disabled = false
	$Menu/Level1/BackLevel.disabled = false
	condition = 3

func _on_Level_2_button_down():
	$Menu/SelectLevel.visible = false
	$"Menu/SelectLevel/Level 1".disabled = true
	$"Menu/SelectLevel/Level 2".disabled = true
	$Menu/SelectLevel/BackMain.disabled = true
	
	$Menu/Level2.visible = true
	$Menu/Level2/Nanora2.disabled = false
	$Menu/Level2/Kate2.disabled = false
	$Menu/Level2/BackLevel.disabled = false
	condition = 3

func _on_Quit_button_down():
		globvar.save_game()
		get_tree().quit()

func _on_CheckBox_toggled(button_pressed):
	if $Menu/Settings/HBoxContainer/VBoxContainer/CheckBox.pressed:
		globvar.god_mode = true
	else:
		globvar.god_mode = false
	globvar.save_game()

func _on_Settings_button_down():
	$Menu/MainOptions.visible = false
	$Menu/MainOptions/NewGame.disabled = true
	$Menu/MainOptions/SelectLevel.disabled = true
	$Menu/MainOptions/Quit.disabled = true
	$Menu/MainOptions/Settings.disabled = true
	
	$Menu/Settings/HBoxContainer/VBoxContainer/CheckBox.disabled = false
	$Menu/Settings/CenterContainer/BackMain2.disabled = false
	$Menu/Settings.visible = true
	condition = 4

func _on_BackMain2_button_down():
	$Menu/MainOptions.visible = true
	$Menu/MainOptions/NewGame.disabled = false
	$Menu/MainOptions/SelectLevel.disabled = false
	$Menu/MainOptions/Quit.disabled = false
	$Menu/MainOptions/Settings.disabled = false
	
	$Menu/Settings/HBoxContainer/VBoxContainer/CheckBox.disabled = true
	$Menu/Settings/CenterContainer/BackMain2.disabled = true
	$Menu/Settings.visible = false
	condition = 1
	globvar.save_game()

func _on_Nanora_button_down():
	get_tree().change_scene_to(storyKate)

func _on_Kate_button_down():
	get_tree().change_scene_to(levelKate2)

func _on_Kate2_button_down():
	get_tree().change_scene_to(levelOsoashi2)

func _on_Nanora2_button_down():
	get_tree().change_scene_to(storyOsoashi)
