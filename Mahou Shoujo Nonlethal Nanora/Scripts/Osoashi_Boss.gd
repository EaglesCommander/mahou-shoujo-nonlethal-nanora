extends KinematicBody2D

var health = 100
var pattern = 0
var pattern1 = 0
var pattern1count = 0
var pattern2 = 0
var pattern22 = 3
var pattern4 = 0
var dashsound = 0
var unlock_flag = false
var invisible = false
var intermission = false
var bullet_scene = load("res://Scenes/EnemyBullet2.tscn")
var bullet_gradual = load("res://Scenes/EnemyBullet3.tscn")
var circle_scene = load("res://Scenes/BulletCircle4.tscn")
var diabolic = load("res://Scenes/EnemyBullet4.tscn")
var circle2 = load("res://Scenes/BulletCircle3.tscn").instance()
var laser_scene = load("res://Scenes/EnemyLaser2.tscn")
var explosion = load("res://Scenes/Explode1.tscn")
var explosion2 = load("res://Scenes/Explode2.tscn")
var bitta = load("res://Scenes/Bitta.tscn")
var beam_circle = load("res://Scenes/BeamCircle.tscn")
var velocity = Vector2()
var isDead = false
const UP = Vector2(0,-1)

onready var animator = self.get_node("Animator")
onready var weapon = self.get_node("Character/Weapon")
onready var enemy = get_parent().get_node("Player")


func _ready():
	$Timer.set_wait_time(0.01)
	$Timer.start()

func true_ready():
	$BulletTimer.set_wait_time(0.1)
	$BulletTimer.start()
	$Timer.set_wait_time(0.1)
	$Timer.start()
	self.add_child(circle2)
	invisible = false
	$Weapon/Book2.visible = true

func _on_BulletTimer_timeout():
	if pattern == 1:
		pattern1()
	elif pattern == 2:
		pattern2()
	elif pattern == 3:
		pattern3()
	elif pattern == 4:
		pattern4()
		
func _on_Timer_timeout():
	if pattern == 0:
		scene_enter()
	if pattern == 1:
		pattern1mov()
	if pattern == 2:
		pattern2mov()
	if pattern == 3:
		pattern3mov()
	if pattern == 4:
		pattern4mov()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	enemy = get_parent().get_node_or_null("Player")
	if enemy == null:
		enemy = get_parent().get_node_or_null("Screen")
	follow_mouse()
	if velocity.x <= 0.0001 and velocity.x >= -0.0001:
		animator.play("Idle")
	elif abs(velocity.x) != 0:
		if velocity.x > 0:
			if self.get_node("Character").flip_h:
				animator.play("Backward")
			else:
				animator.play("Forward")
		else:
			if self.get_node("Character").flip_h:
				animator.play("Forward")
			else:
				animator.play("Backward")
func follow_mouse():
	if enemy.position.x > self.position.x:
		$Character.flip_h = false
#		weapon.flip(false)
#		weapon.position.x = 7
		
	else:
		$Character.flip_h = true
#		weapon.flip(true)
#		weapon.position.x = -7

#	weapon.rotation_degrees = rad2deg(self.position.angle_to_point(enemy.global_position)) + 180

func pattern1():
	$Bullet.play()
		
	for n in range(3):
		var b = bullet_scene.instance()
		b.position = $Weapon/Book.global_position
		b.target = (120 * n) + pattern1count
		get_parent().add_child(b)
		b = bullet_scene.instance()
		b.position = $Weapon/Book2.global_position
		b.target = (120 * n) + pattern1count
		get_parent().add_child(b)
	
	pattern1count += 5
	
	if pattern1count == 360:
		pattern1count = 0
	
func pattern1mov():
	$Weapon.rotate(deg2rad(10))
	$Weapon/Book.rotate(deg2rad(-10))
	$Weapon/Book2.rotate(deg2rad(-10))
	
	if $Weapon.rotation_degrees == 360:
		self.rotation_degrees = 0
		$Weapon/Book.rotation_degrees = 0
		$Weapon/Book.rotation_degrees = 0
	
	if pattern1 == 0:
		$Weapon/Book.position.y -= 5
		$Weapon/Book2.position.y += 5
	else:
		$Weapon/Book.position.y += 5
		$Weapon/Book2.position.y -= 5
	
	if $Weapon/Book.position.y <= -400:
		pattern1 = 1
	if $Weapon/Book.position.y >= -50:
		pattern1 = 0

func pattern2():
	$Bullet.play()
	
	pattern1count += 1
	
	if pattern1count == 360:
		pattern1count = 0
	
	for n in range(4):
		var b = bullet_gradual.instance()
		b.position = self.global_position
		b.target = (90 * n) + pattern1count
		get_parent().add_child(b)
	
func pattern2mov():
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	if pattern2 == 4:
		pattern2 = 0
	
	if pattern2 == 0:
		node = node.get_node("MiddleTop")
	elif pattern2 == 1:
		node = node.get_node("MiddleLeft")
	elif pattern2 == 2:
		node = node.get_node("MiddleBottom")
	elif pattern2 == 3:
		node = node.get_node("MiddleRight")
	
	if abs(self.global_position.x - node.global_position.x) <= 5 && abs(self.global_position.y - node.global_position.y) <= 5:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		pattern2 += 1
		return
		
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 6
	move_and_collide(velocity)
	
func pattern3():
	var d = diabolic.instance()
	d.target = get_angle_to(enemy.global_position)
	d.position = self.global_position
	get_parent().add_child(d)
	
	$Bullet.play()
	
func pattern3mov():
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	if pattern2 == 4:
		pattern2 = 0
	
	if pattern2 == 0:
		node = node.get_node("BottomRight")
	elif pattern2 == 1:
		node = node.get_node("TopRight")
	elif pattern2 == 2:
		node = node.get_node("BottomLeft")
	elif pattern2 == 3:
		node = node.get_node("TopLeft")
	
	if abs(self.global_position.x - node.global_position.x) <= 5 && abs(self.global_position.y - node.global_position.y) <= 5:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		pattern2 += 1
		return
		
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 4
	move_and_collide(velocity)
	
func pattern4():
	for n in range(-1, 2):
		var d = diabolic.instance()
		d.target = get_angle_to(enemy.global_position) + deg2rad(45 * n)
		d.position = self.global_position
		d.explode = false
		get_parent().add_child(d)
	
	$Bullet.play()
	
func pattern4mov():
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	
	if pattern4 == 3:
		pattern4 = 1
		
	if health <= 30 and pattern4 != 0:
		unlock_flag = true

	if pattern4 == 0:
		node = node.get_node("MiddleTop")
	elif pattern4 == 1:
		node = node.get_node("TopRight")
	elif pattern4 == 2:
		node = node.get_node("TopLeft")
	
	if abs(self.global_position.x - node.global_position.x) <= 5 && abs(self.global_position.y - node.global_position.y) <= 5 && pattern4 == 0:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		pattern4 += 1
		$BulletTimer.set_wait_time(3)
		$BulletTimer.start()
		var circle1 = beam_circle.instance()
		circle1.number = 0
		circle1.global_position = get_parent().get_node("Screen").get_node("TopRight").global_position
		circle1.position.x -= 220
		get_parent().add_child(circle1)
		var circle2 = beam_circle.instance()
		circle2.number = 1
		circle2.global_position = get_parent().get_node("Screen").get_node("TopLeft").global_position
		circle2.position.x += 220
		get_parent().add_child(circle2)
		invisible = false
		return
	elif abs(self.global_position.x - node.global_position.x) <= 50 && abs(self.global_position.y - node.global_position.y) <= 50 && pattern4 != 0:
		pattern4 += 1
		return
		
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 4
	move_and_collide(velocity)

func _on_Intermission_timeout():
	$Intermission.stop()
	$Character.modulate.a = 1
	
	if intermission:
		$Intermission.set_wait_time(2)
		$Intermission.start()
		intermission = false
		pattern += 1
		self.add_child(explosion2.instance())
		$Recover.play()
		return
		
	if pattern == 2:
		self.remove_child(circle2)
		$Weapon/Book2.visible = false
		$Weapon/Book.global_position = self.global_position
		health = 100
		$Timer.set_wait_time(0.01)
		$Timer.start()
		$BulletTimer.set_wait_time(0.1)
		$BulletTimer.start()
		var b = bitta.instance()
		b.global_position = self.global_position
		get_parent().add_child(b)
		
	elif pattern == 3:
		$Weapon/Book.visible = false
		health = 100
		$Timer.stop()
		$Timer.set_wait_time(0.01)
		$Timer.start()
		$BulletTimer.set_wait_time(1)
		$BulletTimer.start()
		var circ = circle_scene.instance()
		circ.position = get_parent().get_node("Screen").global_position
		get_parent().add_child(circ)
		
	invisible = false
	
	if pattern == 4:
		$Weapon/Book.visible = true
		health = 100
		$Timer.set_wait_time(0.01)
		$Timer.start()
		$BulletTimer.stop()
		invisible = true
		
func death():
	var ex = explosion.instance()
	ex.position = self.global_position
	get_parent().add_child(ex)
	
	if pattern == 2:
		get_parent().get_node("Bitta").death()
	
	invisible = true
	intermission = true
	
	$Character.modulate.a = 0.7
	$Intermission.set_wait_time(2)
	$Intermission.start()
	$BulletTimer.stop()
	$Timer.stop()
	
	$Death.play()
	
	for x in get_parent().get_children():
		if "EnemyBullet" in x.get_name():
			self.get_parent().remove_child(x)
		if "EnemyLaser" in x.get_name():
			self.get_parent().remove_child(x)
		if "Circle" in x.get_name():
			x.death()
		if "Diabolic" in x.get_name():
			self.get_parent().remove_child(x)
	
	if pattern == 4:
		isDead = true
		$Intermission.stop()
		return

func _on_Core_Fate_area_entered(area):
	if invisible:
		return

	if "EnemyBullet" in area.get_parent().get_name() or "EnemyLaser" in area.get_parent().get_name() or "EnemySword" in area.get_parent().get_name() or "EnemyBeam" in area.get_parent().get_name():
		return
	if "Bullet" in area.get_parent().get_name():
		self.get_parent().remove_child(area.get_parent())
		if pattern == 1:
			health -= 1
		elif pattern == 2:
			health -= 4
		else:
			health -= 2
	if "Beam" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		health -= 50
		$BeamTimer.start()
	if "Sword" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		health -= 70
		$BeamTimer.start()
	if "Laser" in area.get_parent().get_name() && $BeamTimer.is_stopped():
		self.get_parent().remove_child(area.get_parent())
		health -= 5
	if health <= 0:
		death()

func scene_enter():
	invisible = true
	var node = get_parent().get_node("Screen")
	var target = Vector2(0, 0)
	
	if abs(self.global_position.x - node.global_position.x) <= 2 && abs(self.global_position.y - node.global_position.y) <= 2:
		self.global_position.x = node.global_position.x
		self.global_position.y = node.global_position.y
		true_ready()
		pattern += 1
		return
	
	target.x = cos(get_angle_to(node.global_position))
	target.y = sin(get_angle_to(node.global_position))
	velocity = target * 2
	move_and_collide(velocity)
