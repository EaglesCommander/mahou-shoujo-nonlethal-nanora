extends Node2D

var menu = load("res://Scenes/MainMenu.tscn")
var level1 = load("res://Scenes/LevelKate.tscn")
var level1alt = load("res://Scenes/LevelKate2.tscn")
var winstory = load("res://Scenes/LoadingWinKate.tscn")
var winstorysb = load("res://Scenes/LoadingWinKateStarlit.tscn")
var level2alt = load("res://Scenes/LoadingLevelOsoashi2.tscn")
var alternate = false
onready var globvar = get_node("/root/Global")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().get_tree().paused = true
	if get_name() == "Win":
		globvar.osoashi_unlocked = true
	if alternate:
		globvar.kate_unlocked = true
	globvar.save_game()

func _on_TextureButton_button_down():
	pass # Replace with function body.

func _on_Resume_button_down():
	get_parent().get_tree().paused = false
	get_parent().remove_child(self)

func _on_Quit_button_down():
	get_parent().get_tree().quit()

func _on_MainMenu_button_down():
	get_parent().get_tree().paused = false
	get_parent().get_tree().change_scene_to(menu)

func _on_Retry_button_down():
	get_parent().get_tree().paused = false
	if get_parent().charName == "Nanora":
		get_parent().get_tree().change_scene_to(level1)
	else:
		get_parent().get_tree().change_scene_to(level1alt)

func _on_Continue_button_down():
	get_parent().get_tree().paused = false
	if get_parent().charName == "Nanora":
		if alternate:
			get_parent().get_tree().change_scene_to(winstorysb)
		else:
			get_parent().get_tree().change_scene_to(winstory)
	else:
		get_parent().get_tree().change_scene_to(level2alt)
