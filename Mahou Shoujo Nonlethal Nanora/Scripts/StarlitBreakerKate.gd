extends Node2D

var nanora = true

onready var next_scene = load("res://Scenes/LoadingStoryOsoashi.tscn")

func _input(event):
	if event.as_text() == "Escape":
		get_tree().change_scene_to(next_scene)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $Background2.position.x <= -1380:
		$Background2.position.x = $StartPoint.position.x
	if $Background1.position.x <= -1380:
		$Background1.position.x = $StartPoint.position.x
	$Background2.position.x -= 1
	$Background1.position.x -= 1
	
	if $Polygon2D/DialogBox.page == 22 and nanora:
		$StoryTimer.start()
		nanora = false
	
	if $Polygon2D/DialogBox.page == $Polygon2D/DialogBox.dialog.size()-1:
		get_tree().change_scene_to(next_scene)

func _on_StoryTimer_timeout():
	$Screen/Player.modulate.a -= 0.02
	
	if $Screen/Player.modulate.a < 0.01:
		$Screen/Player.modulate.a = 0
		$StoryTimer.stop()


func _on_AudioStreamPlayer_finished():
	$AudioStreamPlayer.stream = load("res://Assets/Music/sunbeams_loop.ogg")
	$AudioStreamPlayer.play()
