extends KinematicBody2D

export (int) var speed = 200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var bullet_scene = load("res://Scenes/Bullet.tscn")
var explosion = load("res://Scenes/Explode1.tscn")

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Character")
onready var weapon = self.get_node("Character/Weapon")
onready var enemy = get_parent().get_node("Enemy")
onready var globvar = get_node("/root/Global")

func get_input(delta):
	velocity.x = 0
	velocity.y = 0
	weapon.stop()
	
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_pressed('up'):
		velocity.y -= speed
	if Input.is_action_pressed('down'):
		velocity.y += speed


func _physics_process(delta):
	get_input(delta)
	velocity = move_and_slide(velocity, UP)

func _process(_delta):
	enemy = get_parent().get_node_or_null("Enemy")
	follow_mouse()
	if velocity.x != 0:
		if velocity.x > 0:
			if self.get_node("Character").flip_h:
				animator.play("Backward")
			else:
				animator.play("Forward")
		else:
			if self.get_node("Character").flip_h:
				animator.play("Forward")
			else:
				animator.play("Backward")
	else:
		animator.play("Idle")

func follow_mouse():
	if get_global_mouse_position().x > self.global_position.x:
		$Character.flip_h = false
		weapon.flip(false)
		weapon.position.x = 7
		
	else:
		$Character.flip_h = true
		weapon.flip(true)
		weapon.position.x = -7
	
	weapon.rotation_degrees = rad2deg(self.global_position.angle_to_point(get_global_mouse_position())) + 180
