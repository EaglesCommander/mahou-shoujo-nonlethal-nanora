extends Node2D

export var weaponSprite = "GrazingHard"
export var weaponCircle = "MagicCircleNanora"
var beam_scene = load("res://Scenes/Beam.tscn")
var beamInstance
var beaming = false
var starlit = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.texture = load("res://Assets/Sprite/" + weaponSprite + ".png")
	$Sprite/Circle.texture = load("res://Assets/Sprite/" + weaponCircle + ".png")
	$Sprite/Circle.visible = false

func fire():
	$Sprite/Circle.visible = true

func stop():
	$Sprite/Circle.visible = false
	if beaming:
		self.remove_child(beamInstance)
		beaming = false

func flip(val):
	$Sprite.flip_h = val
	
func beam():
	if !beaming:
		beamInstance = beam_scene.instance()
		beamInstance.position = $Point.position
		if starlit:
			beamInstance.starlit = true
		self.add_child(beamInstance)
		beaming = true
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
