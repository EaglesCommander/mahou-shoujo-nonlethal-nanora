# Mahou Shoujo Nonlethal Nanora

Personal project for Game Development class of 2020 at University of Indonesia

Entered in - [CSUI Individual Game Jam](https://itch.io/jam/individual-game-jam-csui-2020)

## Description

In a separate continuity, a brave young hero rises. But this one is unlike the others....

"Magic is pretty cool and all, but a lady's life is more important!"

Hikumura Nanora, a third year grade schooler, recently stumbled into magic. Figuring that her powers can also be used to further help her agendas, she sets out to make other people "understand" her feelings. At some times nicely, but at others.... BY FORCE!

"It's okay if I become a devil, as long as I can get you to understand!"

This is the story of friendships and more friendships! Join Nanora in her conquest to "befriend" every ​cute girl she meets in a, sometimes, non-lethal way. But beware, the path to friendship is treacherous. Can you survive the onslaught of the powerful yet cute girls?​ Mahou Shoujo Nonlethal Nanora starts now!

## Concept

Mahou Shoujo Nonlethal Nanora is a 2D horizontal bullet-hell game inspired by Suguri​ and the Nanoha series​. However, unlike most bullet-hells, this game requires the player to actively aim for the enemy which adds to the difficulty. This game will challenge your ability to both dodge the bullets and aim for the enemy at the same time. You will face off with 1 boss with 4 phases each. Each phase will have a 120 seconds timer, if you fail to beat the phase during that time, it's game over. You will have 5 lives on each level.

You may unlock the next level by beating the boss of a previous level, but there are certain conditions that you can fulfill to unlock that boss as a playable character ! (Details below how to play) ​Defeat bosses, enjoy the story, and search around for that unlock condition to use the boss as a player in previous levels! Each boss has unique abilities that may make certain phases easier!

Nanora aims to befriend every cute girl she sees, and it just so happen that every bosses are cute. Nanora needs to help them understand her feelings using her powers. This is derived from a catchphrase from the Nanoha series, "It's okay if I'm a devil... it just means I'll have to use my hellish tools to get you to listen!"​ and the way to unlock each bosses are derived from the series' actual story.

## Diversifiers

This game is submitted in the CSUI Individual Game Jam of 2020 and includes three diversifier provided by the game jam:

- Starlight Breaker (Make your enemies your friend by beating them)

This is the base idea of the whole game. Each boss can be beaten normally, but it won't be enough for them to understand Nanora. To befriend them, you have to hit them with your best move, then and only then will they understand that they ​are​ your friend. The Starlit Breaker.​ To use the Starlit Breaker, you have to fulfill a certain condition unique to each boss. (For unlockable bosses)

- I can’t believe my enemy is this strong​ (Implement an enemy that cannot be defeated in 1 life point cycle)

This is the main idea for the boss phases. Drawing inspirations from Touhou​. Each boss has 4 phases, and each phase you have to reduce the boss' health to 0, like the spellcard system in Touhou. To add to this diversifier, the bosses also has "strong wills", meaning they cannot be defeated normally. An ​ultimate move​ cost 1 life to use, which means to fully "defeat" and "befriend" the enemy, you have to lose 1 life.

- Aku ingin terbang bebas di angkasa ('Flight' is the main way a player can move the character)

This is the main movement type of the game, the game takes place on the sky, where most magical girls fight. The player can move around freely in the 2D plane, and to support that movement, the background scrolls slowly and the character sprites tilts slightly forward and backward like helicopters. But another interpretation of this diversifier is how the main way to play this game is by "fleeing" from the enemy bullets using the space available.

## How to Play

- W A S D to move around the screen
- Move the mouse around to aim
- Click or hold Left Mouse Button to shoot, you will fire a bullet at your mouse's pointer location
- Hold Right Mouse Button to focus, this will cut your movement speed by half
- Press Shift to dash, you're immune to bullets while dashing. There is a cooldown for dashing
- Press space to unleash your most powerful move! This is different per character, the default character will shoot a long range beam. After using this, all bullets on screen will disappear and you will be immune to bullets for a short while,  but you will lose 1 life, so be careful!
- If you get hit by a bullet, you will lose 1 life, but you will gain a temporary immunity and some bullets may disappear. Use this to your advantage by purposefully throwing - yourself near the end of a phase!
- This game offers a god mode option for players who wish to enjoy the story or for players that want to practice. You can still unlock characters and levels with god mode on!
- Each character has their own unique abilities, a level or a phase may be easier with certain characters!

## How to Unlock Bosses

As of right now, only Kate can be unlocked

- Kate: Have only 1 life remaining while Kate is on phase 4 and at under 30% health remaining
- Osoashi: Get her own laser to strike her 5 times from her own beams on phase 4 (Unimplemented)

## External Asset List

Sprites:

- Nanoha, Fate, Hayate and Vita sprites​ (Edited) [here](https://www.deviantart.com/sgtkle)
- Magic Circle​ Michildan​​ (Color edited) [here](https://www.cleanpng.com/png-magic-circle-deviantart-magic-circle-731789/)
- Magic Circle Velkan​ (Color edited) [here](https://www.deviantart.com/feraligono/art/Magic-Circles-Lyrical-Nanoha-Create-swf-Props-557848706)
- Weapons​ (Edited) [here](https://modworkshop.net/mod/26153)
- Explosions ​[here](https://kvsr.itch.io/explosion)
- Bullets and Lasers​ (Color edited) [here](https://gamesupply.itch.io/massive-weapon-package)

Sound Effects (Names are the way they are in the source code):

- ​Vita's swing​ [here](https://opengameart.org/content/swishes-sound-pack)
- Click and Bullet Fire​ [here](https://kenney.nl/assets/ui-pack)
- Various Retro Sounds​ [here](https://kronbits.itch.io/freesfx)
- Digital and Sci-Fi audio​ [here](https://kenney.nl/assets?q=audio)

Music (Names are the way they are in the source code)​:

- The Oath - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_boss.html)
- Unison in the Abyss - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_boss.html)
- Forgotten Place - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_town_field.html)
- Game Over - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_jingle.html)
- Jingle 1 - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_jingle.html)
- Paradise Lost - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_town_field.html)
- Sunbeams - Wingless Seraph​ [here](https://wingless-seraph.net/en/material-music_town_field.html)
- Main Theme 2 ~Tracks of Legends~ - Wingless Seraph​ (Loop edited) [here](https://wingless-seraph.net/en/material-music_title.html)

UI:

- UI button [here](https://kenney.nl/assets/ui-pack)
- ​Caution sign​ (Edited) [here](https://www.deviantart.com/faram45/art/Nuclear-Warning-Sprite-Animation-Transparent-279662879)
- Health background, bar, and font​​ [here](https://docs.godotengine.org/en/stable/getting_started/step_by_step/ui_game_user_interface.html)

Other images:

- Main screen background​ [here](https://www.pexels.com/photo/abstract-art-artistic-background-1448465/)
- Level 1 background​ [here](https://edermunizz.itch.io/free-pixel-art-forest)
- Level 2 background​ [here](https://wallpaperaccess.com/city-pixel-art)
- Nanoha standing (Edited)​ [here](https://www.deviantart.com/dbzandsm/art/Nanoha-Takamachi-Render-383142389)
- Logo​ (Edited) [here](https://pixabay.com/illustrations/eagle-silhouette-bird-eagle-hawk-4892371/)

## Additional notes

- This is my first game of this scale that I have finished, and the first game overall with Godot. 
- I'm a sole developer. Most assets are taken from other sources and inspirations are heavily drawn from Touhou and Suguri.
- The game only offers 2 levels and 1 unlockable character. This game will not be expanded upon with the same assets and code due to lack of assets and experience from the developers side.​
- ​The code is, frankly speaking, a total mess. It is available publicly. Anything in this game that is not an external asset is free to use and improve on.